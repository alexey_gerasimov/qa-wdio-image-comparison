# README #

This repo contains template for image comparison with WebDriverIO and `wdio-image-comparison-service` package

## How do I get set up? ##

Use `npm install` to initialize all packages

All configuration settings you can change in **wdio.conf.js** file or by using `npx wdio config`.

## How do I run it? ##

You can start your test suite by using the run command and pointing to the WebdriverIO config that you just created:

```sh
npx wdio run ./wdio.conf.js
```

If you like to run specific test files you can add a --spec parameter:

```sh
npx wdio run ./wdio.conf.js --spec screenshot-cmp.js
```
or define suites in your config file and run just the test files defined by in a suite:

```sh
npx wdio run ./wdio.conf.js --suite exampleSuiteName
```

## Other info ##

All tests images will be stored in `./baselines/` and `./.tmp/` folders:

- In `./baselines/` directory you can store images with expected results for *Screen/Element/FullPageScreen* screenshots. If image is not added in folder - it will be created after first run
- In `./.tmp/` dyrectory will be created two more folders - `./.tmp/actual/` and `./.tmp/diff` for actual results and differenses wich was found while test

These settings you can change manually in `wdio.conf.js` file.

## Sources: ##

> WebdriverIO - <https://webdriver.io/docs/what-is-webdriverio>

> WDIO Image Comparison Service - <https://webdriver.io/docs/wdio-image-comparison-service/>