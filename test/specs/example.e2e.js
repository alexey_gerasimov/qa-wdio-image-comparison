const LoginPage = require('../pageobjects/login.page');
const SecurePage = require('../pageobjects/secure.page');
const HomePage = require('../pageobjects/home.page');


describe('NextGear UAT1 home page', () => {
    beforeEach(() => {
        HomePage.open();
    });

    it('should be HTTPS site', async () => {
        // await HomePage.open();
        await expect(browser).toHaveUrlContaining('http://');
    });

    it('should save a screenshot with opened page', async () => {
        // await HomePage.open();
        await browser.saveScreenshot('screenshot.png');
        // await expect(SecurePage.flashAlert).toBeExisting();
        // await expect(SecurePage.flashAlert).toHaveTextContaining();
    });

    it('should test some CSS regression screens', async () => {
        await browser.saveScreen('examplePaged', {
            disableCSSAnimation: true,
            hideScrollBars: true,
        });

        await browser.saveFullPageScreen('fullPage', {
            // fullPageScrollTimeout: 3000,
            // disableCSSAnimation: true
        });
    });

    it('should compare successful with a baseline', async () => {
        await expect(browser.checkScreen('examplePaged', {
            blockOut: [{
                height: 10,
                width: 5,
                x: 40,
                y: 65
            }, {
                height: 250,
                width: 500,
                x: 0,
                y: 35
            },
            ],
            ignoreAlpha: true,
            blockOutStatusBar: true,
            disableCSSAnimation: true
        })).toEqual(0);

        // expect(browser.checkElement($('#element-id'), 'firstButtonElement', { /* some options*/ })).toEqual(0);

        await expect(browser.checkFullPageScreen('fullPage', {
            // ignoreColors: true,
            // disableCSSAnimation: true
        })).toEqual(0);
    });

});


